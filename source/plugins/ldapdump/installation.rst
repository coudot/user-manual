Installation
============

Install packages
----------------

Archlinux
^^^^^^^^^

.. code-block:: bash

   yaourt -S fusiondirectory-plugin-ldapdump

Debian
^^^^^^

.. code-block:: bash

   apt-get install fusiondirectory-plugin-ldapdump

RHEL
^^^^

.. code-block:: bash

   yum install fusiondirectory-plugin-ldapdump
 
