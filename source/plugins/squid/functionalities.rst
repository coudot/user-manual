.. include:: /globals.rst

Functionalities
===============

* Create Squid User

Click on Users icon in FusionDirectory

.. image:: images/squid-users.png
   :alt: Picture of Users icon in FusionDirectory

Click on an existing user

.. image:: images/squid-user.png
   :alt: Picture of user line in FusionDirectory

Click on “Proxy” tab

.. image:: images/squid-proxy.png
   :alt: Picture of Proxy tab in FusionDirectory
   
Click on “Add Proxy Settings” button

.. image:: images/squid-proxy-settings.png
   :alt: Picture of Proxy Settings button in FusionDirectory
   
Fill the desired fields then click “Ok” to save   

.. image:: images/squid-proxy-account-settings.png
   :alt: Picture of Proxy Account Settings page in FusionDirectory

Proxy account

   * Filter unwanted content (i.e. pornographic or violence related) : Check if you want enable filtering unwanted content (i.e. pornographic or violence related) for this user.  
   
   * Limit proxy access to working time : Check if you want enable limit proxy access to working time. Choose the start and the end of working time beside, once enabled.  
   
   * Restrict proxy usage by quota : Check if you want enable restrict proxy usage by quota. Choose the quota amount beside, once enabled.  
   
Now, in Properties tab, you can see the squid icon 

.. image:: images/squid-user-squid-icon.png
   :alt: Picture of Squid icon in FusionDirectory  
   
   

