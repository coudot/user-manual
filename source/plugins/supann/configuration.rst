Configuration
=============

The SupAnn standard specifies how to fill cn attribute: https://services.renater.fr/documentation/supann/supann2018/recommandations2018/attributs/cn

You can change how FusionDirectory fill cn attribute by modifying CN Pattern by "%t[fr_FR]|sn% %t[fr_FR]|givenName%" in the configuration backend :ref:`configuration_people_and_group_storage`



