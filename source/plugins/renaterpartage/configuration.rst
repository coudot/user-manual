Configuration
=============

If you need to modify something, you can access to FD configuration of the plugin by the 'Configuration' icon or entry in the 
'Addons' section of the main page of FusionDirectory Configutation Interface: 



.. image:: images/renaterpartage-configuration.png
   :alt: Picture of Renater partage configuration in FusionDirectory
