.. include:: /globals.rst

Functionalities
===============

* Audit list

.. image:: images/audit-view.png
   :alt: Picture of audit list in FusionDirectory

* Create a filter

.. image:: images/audit-filter.png
   :alt: Picture of Audit create a filter in FusionDirectory

* Audit filter result

.. image:: images/audit-filter-result.png
   :alt: Picture of Audit filter result in FusionDirectory
   
* Audit event

.. image:: images/audit-event.png
   :alt: Picture of Audit event in FusionDirectory
   
If you click on the author icon, you will get the author information


* Audit author

.. image:: images/audit-author.png
   :alt: Picture of Audit author in FusionDirectory



