.. FusionDirectory User Manual documentation master file, created by
   sphinx-quickstart on Wed Oct 18 14:30:18 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FusionDirectory User Manual's documentation!
=======================================================

.. image:: /_static/images/fd_logo.png
   :alt: FusionDirectory
   :align: center

Contents:

.. toctree::
   :maxdepth: 2

   whatis/fusiondirectory.rst
   prerequisite/prerequisite.rst
   install/index
   update/index
   core/index
   configuration/index
   acls/index
   plugins/index
   templates/index
   triggers/index
   faq/index.rst
   contribute/bugreport.rst
   release/index
   distribution/index.rst
   support/index.rst
   security/index.rst
   license/index.rst
   authors/index.rst
   contact/contact.rst
   code-of-conduct.rst

.. include:: globals.rst
